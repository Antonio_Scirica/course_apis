<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

    class Course extends Model 
    {
        protected $fillable = [
            'title', 'description', 'identificativo'
        ];

        public function subscription()
        {
            return $this->hasMany('App\Models\Subscription');
        }
    }