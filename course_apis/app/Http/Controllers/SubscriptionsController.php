<?php

    namespace App\Http\Controllers;
    
    use App\Models\Student;

	use Illuminate\Http\Request;

    class SubscriptionsController extends Controller
    {
        public function index(Request $request)
   	    {
      		$subscriptions = Subscription::all();
       		return $this->success($bookings);
   	    }


        public function create(Request $request)
        {
            $this->validate($request, 
            [
            	'student_id' => 'required|numeric|exists:student,id',
         	    'course_id' => 'required|numeric|exists:course,id'
            ]);
            
            $student_id = $request->input('student_id');
            $student_id = $request->input('course_id');
            $subscription = new Subscription();
            $subscription->student_id = $student_id;
            $subscription->course_id = $course_id;
            $subscription->save();

            return $this->success($subscription, 201);
            
        }


        public function destroy(Request $request, $id)
 		{
    		$subscription = Subscription::find($id);

     		if(!$subscription)
      		{
          		return $this->failed('The resource was not found');
       		}
      		else
      		{
         		$subscription->delete();
                return $this->success('The request was deleted');
      		}
        }
    }