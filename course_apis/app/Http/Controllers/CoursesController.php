<?php

    namespace App\Http\Controllers;
    


    use Illuminate\Http\Request;
    use App\Models\Course;

    class CoursesController extends Controller
    {
        public function create(Request $request)
        {
            $this->validate($request, 
            [
            	'title' => 'required|string',
            	'description' => 'string',
            	'identificativo' => 'required|string'
            ]);
            
            $data = $request->all();
            $course = new Course($data);
	        $course->save();
            return $this->success($course, 201);
            
            
        }
         
        public function index(Request $request)
        {
            $courses = Course::all();
            return $this->success($courses);
        }


        public function update(Request $request, $id)
        {
            $this->validate($request, [
                'title' => 'string',
                'description' => 'string',
                'identificativo' => 'string'
            ]);

            $course = Course::find($id);

            if(!$course)
            {
                return $this->failed('The resource was not found');
            }
            else
            {
                $data = $request->all();
                $course->update($data);

                return $this->success($course);
            }

        }

        public function destroy(Request $request, $id)
  	    {
            $course = Course::find($id);

            if(!$course)
            {
                return $this->failed('The resource was not found');
            }
            else
            {
                $course->delete();

                return $this->success('The request was successful');
            }
   	 }

    }