<?php

    namespace App\Http\Controllers;
    
    use App\Models\Student;

	use Illuminate\Http\Request;

    class StudentsController extends Controller
    {
        public function create(Request $request)
        {
            $this->validate($request, 
            [
            	'full_name' => 'required|string',
        		'email' => 'required|email|unique:students,email'
            ]);
            
            $data = $request->all();
            $student = new Student($data);
	        $student->save();
            return $this->success($student, 201);
            
        }
         
    }