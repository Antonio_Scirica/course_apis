<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});



$router->get('/courses', 'CoursesController@index');
$router->post('/courses', 'CoursesController@create');


$router->put('/courses/{id}', 'CoursesController@update');
$router->delete('/courses/{id}', 'CoursesController@destroy');

$router->post('/students', 'StudentsController@create');

$router->post('/subscriptions', 'SubscriptionsController@create');

$router->delete('/subscriptions/{id}', 'SubscriptionsController@destroy');

$router->get('/subscriptions', 'SubscriptionsController@index');





